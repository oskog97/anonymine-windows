@echo off
:: Copyright (c) Oskar Skog, 2022-2024
:: Released under the Simplified (2-Clause) BSD License

:: This is version 18 or newer


:: For Windows 2000 and XP SP2
:: - Remove the --allow-unsupported-windows flag to setup
:: - Mirror:
::     http://ctm.crouchingtigerhiddenfruitbat.org/pub/cygwin/circa/2013/06/04/121035
:: - setup version: 2.774
::     http://ctm.crouchingtigerhiddenfruitbat.org/pub/cygwin/setup/snapshots/setup-2.774.exe
:: This has not been tested


:: Move into script containing directory
cd /d "%0\.."

cls

set unattended_mode=--quiet-mode
if "%1" == "retry" (
    set unattended_mode=
    echo Cygwin installation failed, retrying in attended mode
    echo.
)

:: User mode has not been tested
set mode=admin
:: set mode=user

:: 'lib' to use patched cygwin1.dll, 'reg' to set proc_retry in CYGWIN env var
:: proc_retry doesn't seem to help, recreate "mmaps after fork" fails seemingly
:: without any attempt to retry
set workaround=lib
:: set workaround=
:: set workaround=reg

:: Where you want Cygwin to be installed: (hardcoded path)
:: This is the default path of the installer
if %mode% == admin (
    set cygwin_dir=C:\cygwin
) else (
    set cygwin_dir=%USERPROFILE%\cygwin
)

:: The newest version of setup-x86.exe still "supports" Windows XP SP3.
set src=cygwin\setup-x86.exe
set mirror=http://ctm.crouchingtigerhiddenfruitbat.org/pub/cygwin/circa/2016/08/30/104223/

:: fork(2) sometimes fails on ReactOS.
:: This version will try 50 times before giving up:
set reliable_cygwin1dll=https://github.com/oskar-skog/hack-cygwin252/raw/master/release/hardened-fork/cygwin1.dll

:: /setup in Cygwin:
set cygwin_setup=%cygwin_dir%\setup.exe

if not exist stage2 (
    echo ERROR: Can't find the rest of the files
    echo You need to extract all files from the zip file.
    echo.
    pause
    exit 1
)

:: Will not check for Admin right as "net session" hasn't been implemented yet
:: in ReactOS.

echo.
echo  ***************************************************************************
echo  *                                                                         *
echo  * WARNING:                                                                *
echo  *   This will install an old unsupported version of Cygwin and download   *
echo  *   the files over an insecure connection without verifying signatures.   *
echo  *                                                                         *
echo  * Windows XP SP3 / Server 2003:                                           *
echo  *   - Make sure Internet Explorer is not in "Work Offline" mode           *
echo  *                                                                         *
echo  * ReactOS:                                                                *
echo  *   - ReactOS 0.4.15 (nightly build) or newer required                    *
echo  *   - Cygwin is not working on btrfs                                      *
if %workaround% == reg (
echo  *   - You should log off and log back on again before playing             *
)
echo  *   - Python may have severe issues with fork, this can be resolved by    *
echo  *     reinstalling Python through the Update Anonymine shortcut           *
echo  *                                                                         *
echo  ***************************************************************************
echo.
echo Bugs can be reported to https://gitlab.com/oskog97/anonymine-windows/issues
pause
echo.
echo.

copy %src% setup.exe
echo.

if not %workaround% == reg (
    goto skip_set_reg
)
:: Setting it in the registry will not immediately update the environment
:: variable in running processes.  %CYGWIN% is both the environment variable
:: for this session and a local variable in the below code.
set CYGWIN=proc_retry:250 export
if %mode% == admin (
    set regkey=HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment
) else (
    set regkey=HKEY_CURRENT_USER\Environment
)
reg query "%regkey%" /v CYGWIN >NUL 2>NUL && (
    echo Environment variable "CYGWIN" already set
    reg query "%regkey%" /v CYGWIN
    echo Wanted to set it to "%CYGWIN%"
    echo Please add or adjust "proc_retry" if needed
    echo.
    pause
) || (
    reg add "%regkey%" /v CYGWIN /d "%CYGWIN%" || (
        echo Failed to set environment variable "CYGWIN"
        echo Please set it to "%CYGWIN%"
        echo.
        pause
    )
)
:skip_set_reg

:: Install Cygwin
:: A few things in %cygwin_dir% may be created even if installation fails,
:: so check for bash.exe or something instead of just the base directory.
if exist %cygwin_dir%\bin\bash.exe (
    echo Cygwin appears to be pre-installed.
    echo If the installation has failed, you can remove %cygwin_dir%
    echo and run this batch file again.
    echo.
    pause
) else (
    echo Installing Cygwin to %cygwin_dir%
    echo.
    echo Click next until it's done.  You do not need to select any
    echo additional packages, just click next on that stage.
    echo.
    pause
    if %mode% == admin (
        .\setup --allow-unsupported-windows -X -O -s %mirror% ^
                --wait %unattended_mode% --root %cygwin_dir%
    ) else (
        .\setup --allow-unsupported-windows -X -O -s %mirror% ^
                --no-admin %unattended_mode% --root "%cygwin_dir%"
    )
    if %workaround% == lib (
        echo.
        echo.
        echo Downloading more reliable cygwin1.dll on ReactOS, will fail on XP...
        dwnl %reliable_cygwin1dll% %cygwin_dir%\bin\cygwin1.dll
        echo.
    )
)

if not exist %cygwin_dir%\bin\bash.exe (
    del setup.exe
    cmd /c "Install-on-XP.bat retry"
    exit /b
)

:: Ensure that /setup.exe exists in Cygwin
if exist %cygwin_setup% (
    del setup.exe
) else (
    move setup.exe %cygwin_setup%
)

:: bash -l (login) to use the Cygwin $PATH rather than the MS %PATH%.
:: Then you'll need to go back to $OLDPWD.
:: First call to `/bin/bash -lc` creates your dotfiles, but apparently
:: fails to execute the requested command.
"%cygwin_dir%\bin\bash" -lc true
"%cygwin_dir%\bin\bash" -lc 'cd "$OLDPWD"; ./stage2 %mode% install'
