Table of contents
=================

    Notes
    Step 1  Extract folder and KEEP it (important)
    Step 2  Start batch files
    Step 3  Choose mirror and click next a bunch of times
    Step 4  Answer a few yes/no questions and click next a bunch more times
    Step 5  Installed
    Troubleshooting
    ------------------------------------------------------------------------
    Manual installation
    Inner workings


Notes
=====

    Cygwin will take up quite a lot of space, nearly 1 GB.

    Requirements:
        - Windows 8.1 or newer on x86-64
        - Windows XP SP3 on x86-32, but using a really old version of Cygwin
        - Windows 11 on ARM64 (Emulates x86-64)   NOT TESTED

    See https://cygwin.com/install.html#unsupported for the right mirror
    to choose for the latest version of Cygwin on unsupported Windows versions.


Step 1 (Important)
==================

    Extract the 'anonymine-windows' folder out from the zip file
    and place it somewhere "convenient", eg. the Documents folder.
    
    The "important" thing is that it won't get deleted (Downloads is probably
    not a good place), and won't take up precious screen space (Desktop is
    also proabably not a good place).
    
    This folder will contain a bunch of stuff, including the update/uninstall
    script, if the folder is moved that will stop working.  Anonymine will
    work if this folder is removed, but the update and uninstall shortcuts
    will not.


Step 2
======

    There are three batch files, use the appropriate one.
    If you already have Cygwin installed in one of the known locations, the
    existing installation will be used.

    Install-as-normal-user.bat
        - Cygwin in C:\Users\<username>\cygwin
        - Shortcuts only for you

    Install-as-admin.bat
        - Cygwin in C:\cygwin64 (or C:\cygwin if it exists)
        - Shortcuts installed for all users
        - Must be run as admin

    Install-on-XP.bat
        - For Windows XP / 2003 or ReactOS

    Windows Defender SmartScreen
    ============================

        Click "More info" to get the option to run the batch file.


Step 3
======

    The batch file should now have started, it will notify you about any known
    issues, then it will install Cygwin on your computer.

    Cygwin's installer will ask for a bunch of things:
        - You need to select a mirror, choose one that seems close.
            - If you get errors later on, try going back and choose another
              mirror.
        - It will let you select / change the selection of packages to install.
          This is not needed, just click next.
        - Desktop and Start menu icons, these are for Cygwin's terminal, you
          don't need them.

    Unsupported Windows
    ===================

        Unsupported versions of Windows should be automatically detected
        by the batch files, but you will need to specify a mirror manually.

        https://cygwin.com/install.html#unsupported

        This will install an old unsupported version of Cygwin and download
        the files over an insecure connection without verifying signatures.


Step 4
======

    Cygwin will now run a bash script that is going to ask whether or not
    you'd like shortcuts to be installed on the desktop and start menu, just
    answer yes or no.

    Cygwin's installer should be started automatically to install a few needed
    packages, you may need to select the mirror again.
      When the packages have been installed, the bash script will install the
    game in just a few seconds. If something unexpected occurs, there may be a
    few other questions.
        
    There should now exist whatever shortcuts you decided to install.
    If they don't, you'll have to copy them from this folder and paste them
    where they belong.


Step 5
======

    The game can now be launched with the 'anonymine' command in the
    Cygwin Terminal, and from any shortcuts you installed.

    Updating and uninstalling can be done with the shortcuts.

    Uninstallation is unable to remove Cygwin, but will tell you were it is
    so you can delete it yourself.


Troubleshooting
===============

    1. See below for specific issues

    2. See if there are any updates to this README.  "Update Anonymine"
       will automatically download the newest README.

    3. File a bug: https://gitlab.com/oskog97/anonymine-windows/-/issues/new
       or email oskar@oskog97.com
       Make sure to attach the log file "log"

    [Warnings/Errors during Cygwin installation]
      These errors/warnings may occur but can safely be ignored:
        - Error with ca-certificates
        - Error with openssl
        - Message box about libusb driver

    [Windows Defender SmartScreen]
       See step 2

    [No mirrors list]
       See step 3

    [Warnings about filesystem not supporting hardlinks]
      The stage2 script can't update itself if the filesystem doesn't
      support hardlinks.  It's not important, so you can just ignore it.

      The newest version of stage2 will be saved as "stage2-next" and you
      can replace stage2 with it.  You can start Explorer in the right
      directory from special options.

      False positives may happen if certain files exist. If you didn't create
      those files, you can safely delete them.  (Files: "foo", "bar")

    [Persistent fork failures on ReactOS when running Anonymine]
      If you get consistent fork failures, you can try running rebaseall and
      reinstalling Python, or changing Python version if it doesn't help.
      Start "Update Anonymine" and answer yes for special options, there you
      will find options to fix and change Python.

      Last time I checked, rebaseall followed by reinstallation of Python 3
      fixed the issue.  But now it will surely refuse to work out of spite.
      For further troubleshooting see
      https://what-if.xkcd.com/imgs/a/131/microwave.png

    [Silently fails to install on Windows XP]
      Symptoms:
        Cygwin setup can't download anything
        Messages in the console (if you can catch them):
            connection error: 2 fetching http://ctm.crouchingtigher...
      Cause:
        Internet Explorer is in "Work Offline" mode


==============================================================================
==============================================================================


Manual installation
===================

    1. Install Cygwin, but read step 2 first
    
    2. During the installation, make sure to select "git", "make" and
       "python3" for installation. And optionally a text editor such as "nano".
    
    3. Start the Cygwin terminal
    
    4. Copy paste the following commands into the terminal
            git clone https://gitlab.com/oskog97/anonymine
            cd anonymine
            ./configure -v
            make
            make install

    5. The "anonymine" command will start the game.

    6. If you want, you can create a shortcut to Cygwin's bin\bash.exe
       with the command line arguments "-lc anonymine"

    7. To update, start the terminal
            cd anonymine
            make uninstall
            git pull
            ./configure -v
            make
            make install


Inner workings
==============

    Read the notes from the above sections.
    
    The game didn't use to work on native Windows and still has a few
    drawbacks, so this will install Cygwin automatically.

    The installer will install Cygwin, but only if it can't find it in the
    expected place (= C:\Users\<yourname>\Cygwin or C:\Cygwin64 for admin).
    The Cygwin installer setup.exe is saved in the root of the Cygwin directory
    and will be used to automatically install dependencies.
    
    After Cygwin (+python3, git and make) are installed, it will download
    Anonymine from Gitlab and install it.  You can read
    anonymine\doc\INSTALL.txt
    
    
    Files
    =====

      * shortcuts/Anonymine.lnk             Shortcuts.  These are always
      * shortcuts/Update Anonymine.lnk      created.  (May be useful if you
      * shortcuts/Uninstall Anonymine.lnk   choose to not install any on the
                                            desktop or menu.)

      * *%3a%2f%2f*/                Cygwin downloads directory
                                    (temporary files/cache)
    
      * anonymine/                  Cloned from Gitlab
      * anonymine-backup/           Only exists during updates (and install)
      
      * log                         Logfile, can be useful if there were
                                    errors.
      
        Install-as-normal-user.bat  Cygwin in %USERPROFILE%\cygwin, shortcuts
                                    installed for this user only
      
        Install-as-admin.bat        Cygwin in C:\cygwin64, shortcuts installed
                                    for all users

        Install-on-XP.bat           For XP SP3 / 2003, Cygwin in C:\cygwin,
                                    shortcuts installed for all users
        
        stage2                      The Bash script that does everything
                                    except install Cygwin.

      * stage2-next                 Newest version of this Bash script
                                    downloaded from Gitlab.  This file will
                                    only exist while updating, or if updating
                                    fails.

      * stage2.ver-*                Backups of this Bash script
        
        icon.ico                    Icon for shortcuts

        cygwin/setup-x86_64.exe     64-bit installer for Cygwin (GPL)
        cygwin/setup-x86.exe        32-bit installer for Cygwin (GPL)
        cygwin/COPYING.txt          GPLv2
        cygwin/SOURCE-CODE.txt      Link to the source code for setup-*.exe

      * foo                         Test file to test hardlink support
      * bar                         Should only exist briefly

        LICENSE.txt

      * means that the file/folder is created during installation and doesn't
      exist in the zip file.
        
      If you downloaded from Gitlab, there will be other files as well:
        
        .gitignore
        cygwin/fetch-cygwin
        mkzip
