@echo off
:: Copyright (c) Oskar Skog, 2016-2024
:: Released under the Simplified (2-Clause) BSD License

:: This is version 18 or newer


:: Move into script containing directory
cd /d "%0\.."

cls

set unattended_mode=--quiet-mode
if "%1" == "retry" (
    set unattended_mode=
    echo Cygwin installation failed, retrying in attended mode
    echo.
)

:: Where you want Cygwin to be installed: (hardcoded path)
set cygwin_dir=%userprofile%\cygwin

set src64=cygwin\setup-x86_64.exe
set src32=cygwin\setup-x86.exe

if %PROCESSOR_ARCHITECTURE% == x86 (
    set src=%src32%
    set unsupported=--allow-unsupported-windows --no-verify
) else (
    set src=%src64%
    set unsupported=
)

:: /setup in Cygwin:
set cygwin_setup=%cygwin_dir%\setup.exe

:: Use --allow-unsupported-windows if the version is too old.
:: https://stackoverflow.com/questions/13212033/get-windows-version-in-a-batch-file/
:: Do NOT use setlocal...endlocal, it breaks the batch file on Windows 7!
for /f "tokens=4-7 delims=[.] " %%i in ('ver') do @(
    if %%i==Version (
        set major=%%j
        set minor=%%k
    ) else (
        set major=%%i
        set minor=%%j
    )
)
if %major% == 6 (
    if %minor% lss 3 (
        set unsupported=--allow-unsupported-windows --no-verify
    )
)
if %major% lss 6 (
    :: Install-on-XP.bat assumes admin
    set unsupported=--allow-unsupported-windows --no-verify
)


if not exist stage2 (
    echo ERROR: Can't find the rest of the files
    echo You need to extract all files from the zip file.
    echo.
    pause
    exit 1
)

echo.
echo  ***************************************************************************
echo  *                                                                         *
echo  * Windows 10 (64-bit), Windows 11:                                        *
echo  *   - No known issues                                                     *
echo  *                                                                         *
echo  * Windows 8.1 (64-bit):                                                   *
echo  *   - Untested                                                            *
echo  *                                                                         *
echo  * Older versions and 32-bit are not supported by Cygwin 3.5 and newer     *
echo  *   - See https://cygwin.com/install.html#unsupported                     *
echo  *   - Warning: signatures will not be verified                            *
echo  *                                                                         *
echo  ***************************************************************************
echo.
echo Bugs can be reported to https://gitlab.com/oskog97/anonymine-windows/issues
pause
echo.
echo.


copy %src% setup.exe
echo.

:: A few things in %cygwin_dir% may be created even if installation fails,
:: so check for bash.exe or something instead of just the base directory.
if exist %cygwin_dir%\bin\bash.exe (
    echo Cygwin appears to be pre-installed.
    echo If the installation has failed, you can remove %cygwin_dir%
    echo and run this batch file again.
    echo.
    pause
) else (
    echo Installing Cygwin to %cygwin_dir%
    echo.
    echo You'll need to select a mirror for Cygwin when asked, but otherwise
    echo just click next until it's done.  You do not need to select any
    echo additional packages, just click next on that stage.
    echo.
    pause
    .\setup %unsupported% --no-admin %unattended_mode% --root "%cygwin_dir%"
)

if not exist %cygwin_dir%\bin\bash.exe (
    del setup.exe
    cmd /c "Install-as-normal-user.bat retry"
    exit /b
)

:: Ensure that /setup.exe exists in Cygwin
if exist "%cygwin_setup%" (
    del setup.exe
) else (
    move setup.exe "%cygwin_setup%"
)

:: bash -l (login) to use the Cygwin $PATH rather than the MS %PATH%.
:: Then you'll need to go back to $OLDPWD.
:: First call to `/bin/bash -lc` creates your dotfiles, but apparently
:: fails to execute the requested command.
"%cygwin_dir%\bin\bash" -lc true
"%cygwin_dir%\bin\bash" -lc 'cd "$OLDPWD"; ./stage2 user install'
